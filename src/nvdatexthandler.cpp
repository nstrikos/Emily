/*
 * Explanation: handleText() gets the incoming raw text
 * and for every normalized text that is extracted,
 * sendText() is called to send normalized text to m_receiver
*/

#include "nvdatexthandler.h"
#include "constants.h"

NvdaTextHandler::NvdaTextHandler()
{
    m_receiver = nullptr;
}

void NvdaTextHandler::setReceiver(NvdaTextHandlerIface *receiver)
{
    m_receiver = receiver;
}

void NvdaTextHandler::handleText(QString incomingText)
{
    //This code handles incoming text from nvda    
    QString text = incomingText;

    if (text != "") {
        if (text.contains(nvdaIndex)) {
            bool done = false;
            while (!done) {
                int indexPosition = text.indexOf(nvdaIndex);
                QString readingText = text.left(indexPosition);
                indexPosition = indexPosition + nvdaIndex.size();
                int markPosition = text.indexOf("#");
                QString indexString = text.mid(indexPosition, markPosition - indexPosition);
                text = text.right(text.size() - markPosition - 1);
                sendText(readingText, indexString);
                if (text == "")
                    done = true;
            }
        } else {
            sendText(incomingText, "");
        }
    }
}

void NvdaTextHandler::sendText(QString text, QString index)
{
    if (m_receiver != nullptr)
        m_receiver->handleNormalizedText(text, index);
}

NvdaTextHandler::~NvdaTextHandler()
{

}
