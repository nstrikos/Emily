/*
 * Explanation: This class does nothing important, it delegates
 * the requests to PlayerImpl, which implements the functionality
*/

#include "player.h"
#include <QFile>

Player::Player()
{
    m_indexHandler = nullptr;
    m_buffer = nullptr;
    m_mediaPlayer = new QMediaPlayer();
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    m_audioOutput = new QAudioOutput;
    m_mediaPlayer->setAudioOutput(m_audioOutput);
    m_audioOutput->setVolume(50);
    connect(m_mediaPlayer, &QMediaPlayer::playbackStateChanged, this, &Player::play);
#else
    connect(m_mediaPlayer, &QMediaPlayer::stateChanged, this, &Player::play);
#endif
    m_mediaPlayer->setPlaybackRate(1.0);
}

void Player::addPlaylist(QIODevice *buffer, QString index, QString filename)
{
    m_createdIndexes << index;
    m_createdBuffers.append(buffer);
    m_filenames << filename;
    play();
}

void Player::play()
{
    //If player is in stopped state
    //take one buffer from createdBuffers
    //append it to playedBuffers
    //play it
    //delete played buffers
    //Finally send index to nvda
    //When finished playing the function will be called again
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    if (m_mediaPlayer->playbackState() == QMediaPlayer::StoppedState) {
#else
    if (m_mediaPlayer->state() == QMediaPlayer::StoppedState) {
#endif

        //This is a way to know if player has played a file
        //or if it starts now. In the first case the sizes of
        //m_createdBuffers and m_createdIndexes will differ
        if (m_createdBuffers.size() != m_createdIndexes.size()) {
            clearPlayedBuffers();
            sendIndexToNVDA();
        }

        if (!m_createdBuffers.isEmpty()) {
            m_buffer = m_createdBuffers.takeFirst();
            m_playedBuffers.append(m_buffer);
            m_playedFilenames.append(m_filenames.takeFirst());

            if (dynamic_cast<QBuffer*>(m_buffer)) {
                m_buffer->open(QIODevice::ReadOnly);
                m_buffer->seek(qint64(0));
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
                m_mediaPlayer->setSourceDevice(m_buffer);
#else
                m_mediaPlayer->setMedia(QMediaContent(), m_buffer);
#endif
            } else if (dynamic_cast<QFile*>(m_buffer)) {
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
                m_mediaPlayer->setSource(QUrl::fromLocalFile(m_playedFilenames.last()));
#else
                m_mediaPlayer->setMedia(QUrl::fromLocalFile(m_playedFilenames.last()));
#endif
            }

            m_mediaPlayer->play();
            clearPlayedBuffers();
        }
    }
}

void Player::clearPlayedBuffers()
{
    // Delete all buffers except the buffer that is in m_mediaPlayer
    // If this buffer is deleted m_mediaPlayer will crash
    // causing application to crash
    for (int k = 0; k < m_playedBuffers.size(); k++) {
        QIODevice *tempBuffer = m_playedBuffers.at(k);
        QString filename = m_playedFilenames.at(k);
        if ( tempBuffer != m_buffer) {
            if ( tempBuffer != nullptr) {
                delete tempBuffer;
                tempBuffer = nullptr;
                if (QFile::exists(filename))
                    QFile::remove(filename);
            }
            m_playedBuffers.removeAt(k);
            m_playedFilenames.removeAt(k);
        }
    }
}

void Player::sendIndexToNVDA()
{
    if (!m_createdIndexes.isEmpty()) {
        QString indexToSend = m_createdIndexes.takeFirst();
        if (m_indexHandler != nullptr)
            m_indexHandler->sendIndex(indexToSend);
    }
}

void Player::stop()
{
    clearCreatedBuffers();
    m_createdIndexes.clear();
    clearPlayedBuffers();

    while(!m_filenames.isEmpty()) {
        QString filename = m_filenames.takeFirst();
        if (QFile::exists(filename))
            QFile::remove(filename);
    }

    m_mediaPlayer->stop();
    //m_mediaPlayer->setSource(QUrl(""));
}

void Player::clearCreatedBuffers()
{
    while (!m_createdBuffers.isEmpty()) {
        QIODevice *tempBuffer = m_createdBuffers.takeFirst();
        if (tempBuffer != nullptr) {
            delete tempBuffer;
            tempBuffer = nullptr;
        }
    }

    while(!m_filenames.isEmpty()) {
        QString filename = m_filenames.takeFirst();
        if (QFile::exists(filename))
            QFile::remove(filename);
    }
}

void Player::resume()
{
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    if (m_mediaPlayer->playbackState() == QMediaPlayer::PausedState)
#else
    if (m_mediaPlayer->state() == QMediaPlayer::PausedState)
#endif
        m_mediaPlayer->play();
}

void Player::pause()
{
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    if (m_mediaPlayer->playbackState() == QMediaPlayer::PlayingState)
#else
    if (m_mediaPlayer->state() == QMediaPlayer::PlayingState)
#endif
        m_mediaPlayer->pause();
}

void Player::setIndexHandler(PlayerIface *indexHandler)
{
    m_indexHandler = indexHandler;
}

Player::~Player()
{
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    delete m_audioOutput;
#endif
    delete m_mediaPlayer;
    clearCreatedBuffers();
    clearPlayedBuffers();

    //We delete the last buffer, this buffer is in mediaPlayer.setMedia
    //If we delete this buffer, the application will crash
    //So we first delete mediaPlayer and then we delete m_buffer
    if (m_buffer != nullptr) {
        delete m_buffer;
        m_buffer = nullptr;
    }

    for (int i = 0; i < m_filenames.size(); i++)
        QFile::remove(m_filenames.at(i));
    for (int i = 0; i < m_playedFilenames.size(); i++)
        QFile::remove(m_playedFilenames.at(i));
}
