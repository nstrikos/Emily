#-------------------------------------------------
#
# Project created by QtCreator 2013-05-10T11:21:36
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets network widgets

TARGET = emily
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    downloadmanager.cpp \
    maryserver.cpp \
    net/connection.cpp \
    net/netClient.cpp \
    net/netServer.cpp \
    net/peermanager.cpp \
    player.cpp \
    choosediskdialog.cpp \
    copythread.cpp \
    progressdialog.cpp \
    hotkeythread.cpp \
    nvdatexthandler.cpp \
    nvdasender.cpp \
    settings.cpp \
    nvdacommandreceiver.cpp \
    textmediator.cpp \
    commandmediator.cpp \
    nvdatextreceiver.cpp

HEADERS  += mainwindow.h \
    downloadmanager.h \
    maryserver.h \
    net/connection.h \
    net/netClient.h \
    net/netClientIface.h \
    net/netServer.h \
    net/peermanager.h \
    player.h \
    choosediskdialog.h \
    copythread.h \
    progressdialog.h \
    constants.h \
    hotkeythread.h \
    nvdasender.h \
    settings.h \
    nvdacommandreceiver.h \
    textmediator.h \
    commandmediator.h \
    nvdatexthandler.h \
    nvdatextreceiver.h \
    nvdatextreceiveriface.h \
    nvdatexthandleriface.h \
    downloadmanageriface.h \
    playeriface.h \
    nvdacommandreceiveriface.h \
    settingsiface.h \
    settingsstorageiface.h

FORMS    += mainwindow.ui \
    choosediskdialog.ui \
    progressdialog.ui

TRANSLATIONS = emily_en.ts \
               emily_el.ts

win32:LIBS += -lpsapi

RESOURCES = emily.qrc

RC_ICONS = sound.ico
