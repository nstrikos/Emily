#include <QtNetwork>

#include "netClient.h"
#include "connection.h"
#include "peermanager.h"

NetClient::NetClient()
{
    clientIface = nullptr;
    peerManager = new PeerManager(this);
    peerManager->setServerPort(server.serverPort());
    peerManager->startBroadcasting();

    QObject::connect(peerManager, SIGNAL(newConnection(Connection*)),
                     this, SLOT(newConnection(Connection*)));
    QObject::connect(&server, SIGNAL(newConnection(Connection*)),
                     this, SLOT(newConnection(Connection*)));
}

void NetClient::setClientIface(NetClientIface *iface)
{
    clientIface = iface;
}

void NetClient::sendTextToSpeech(QString text, QString index)
{
    qDebug() << index << text;
    sendMessage("command-text-index:" + index + ":" + text);
}

void NetClient::stop()
{
    sendMessage("command-stop");
}

void NetClient::pause()
{
    sendMessage("command-pause");
}

void NetClient::resume()
{
    sendMessage("command-resume");
}

void NetClient::setRate(QString rate)
{
    double a = rate.toDouble() ;
    double speechRate = 0.02 * a - 1;
    QString newRate = QString::number(speechRate);
    sendMessage("command-rate:" + newRate);
}

void NetClient::setPitch(QString pitch)
{
    double a = pitch.toDouble() ;
    double calcPitch = 0.02 * a - 1;
    QString newPitch = QString::number(calcPitch);
    sendMessage("command-pitch:" + newPitch);
}

NetClient::~NetClient()
{
    delete peerManager;
}

void NetClient::sendMessage(const QString &message)
{
    if (message.isEmpty())
        return;

    QList<Connection *> connections = peers.values();
    foreach (Connection *connection, connections)
        connection->sendMessage(message);
}

QString NetClient::nickName() const
{
    return peerManager->userName() + '@' + QHostInfo::localHostName()
            + ':' + QString::number(server.serverPort());
}

bool NetClient::hasConnection(const QHostAddress &senderIp, int senderPort) const
{
    if (senderPort == -1)
        return peers.contains(senderIp);

    if (!peers.contains(senderIp))
        return false;

    QList<Connection *> connections = peers.values(senderIp);
    foreach (Connection *connection, connections) {
        if (connection->peerPort() == senderPort)
            return true;
    }

    return false;
}

void NetClient::newConnection(Connection *connection)
{
    connection->setGreetingMessage(peerManager->userName());

    connect(connection, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(connectionError(QAbstractSocket::SocketError)));
    connect(connection, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(connection, SIGNAL(readyForUse()), this, SLOT(readyForUse()));
}

void NetClient::readyForUse()
{
    Connection *connection = qobject_cast<Connection *>(sender());
    if (!connection || hasConnection(connection->peerAddress(),
                                     connection->peerPort()))
        return;

    connect(connection, SIGNAL(newMessage(QString,QString)),
            this, SIGNAL(newMessage(QString,QString)));
    connect(connection, SIGNAL(newMessage(QString,QString)),
            this, SLOT(getMessage(QString,QString)));

    peers.insert(connection->peerAddress(), connection);
    QString nick = connection->name();
    if (!nick.isEmpty()) {
        emit newParticipant(nick);
        emit clientConnected();
        peerManager->stopBroadCasting();
    }
}

void NetClient::getMessage(QString from, QString message)
{
    Q_UNUSED(from);
    qDebug() << message;
    if (message.contains("index-finished:")) {
        QString text = "index-finished:";
        int length = text.length();
        QString index = message.right(message.length() - length);
        if (clientIface != nullptr)
            clientIface->newIndex(index);
    }
}

void NetClient::disconnected()
{
    if (Connection *connection = qobject_cast<Connection *>(sender()))
        removeConnection(connection);
}

void NetClient::connectionError(QAbstractSocket::SocketError /* socketError */)
{
    if (Connection *connection = qobject_cast<Connection *>(sender()))
        removeConnection(connection);
}

void NetClient::removeConnection(Connection *connection)
{
    if (peers.contains(connection->peerAddress())) {
        peers.remove(connection->peerAddress());
        QString nick = connection->name();
        if (!nick.isEmpty()) {
            emit participantLeft(nick);
            emit clientDisconnected();
            peerManager->startBroadcasting();
        }
    }
    connection->deleteLater();
}
