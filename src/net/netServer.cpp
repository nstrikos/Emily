#include <QtNetwork>

#include "connection.h"
#include "netServer.h"

NetServer::NetServer(QObject *parent)
    : QTcpServer(parent)
{
    listen(QHostAddress::Any);
}

void NetServer::incomingConnection(qintptr socketDescriptor)
{
    Connection *connection = new Connection(socketDescriptor, this);
    emit newConnection(connection);
}
