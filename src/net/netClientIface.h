#ifndef NETCLIENTIFACE_H
#define NETCLIENTIFACE_H

class NetClientIface
{
public:
    //virtual void clientConnected() = 0;
    //virtual void clientDisconnected() = 0;
    virtual void newIndex(const QString &index) = 0;
};

#endif // NETCLIENTIFACE_H
