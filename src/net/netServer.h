#ifndef NETSERVER_H
#define NETSERVER_H

#include <QTcpServer>

class Connection;

class NetServer : public QTcpServer
{
    Q_OBJECT

public:
    NetServer(QObject *parent = 0);

signals:
    void newConnection(Connection *connection);

protected:
    void incomingConnection(qintptr socketDescriptor) override;
};

#endif
