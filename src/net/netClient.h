#ifndef NETCLIENT_H
#define NETCLIENT_H

#include <QAbstractSocket>
#include <QHash>
#include <QHostAddress>

#include "netServer.h"
#include "netClientIface.h"

class PeerManager;

class NetClient : public QObject
{
    Q_OBJECT

public:
    NetClient();
    ~NetClient();
    void setClientIface(NetClientIface *iface);
    QString nickName() const;
    bool hasConnection(const QHostAddress &senderIp, int senderPort = -1) const;
    void sendTextToSpeech(QString text, QString index);
    void stop();
    void pause();
    void resume();
    void setRate(QString rate);
    void setPitch(QString pitch);

public slots:
    void sendMessage(const QString &message);

signals:
    void newMessage(const QString &from, const QString &message);
    void newParticipant(const QString &nick);
    void participantLeft(const QString &nick);
    void clientConnected();
    void clientDisconnected();
    void newIndex(QString index);

private slots:
    void newConnection(Connection *connection);
    void connectionError(QAbstractSocket::SocketError socketError);
    void disconnected();
    void readyForUse();
    void getMessage(QString from, QString message);

private:
    void removeConnection(Connection *connection);

    PeerManager *peerManager;
    NetServer server;
    QMultiHash<QHostAddress, Connection *> peers;
    NetClientIface *clientIface;
};

#endif
