#include "settings.h"

#include <QLocale>
#include <QSettings>
#include <QDir>
#include <QTextStream>

#include "constants.h"

Settings::Settings()
{
    m_settingsUpdater = nullptr;
}

void Settings::setUpdater(SettingsIface *updater)
{
    m_settingsUpdater = updater;
}

void Settings::readSettings()
{
    QString dir =  QDir::currentPath();

    if (dir.startsWith("c", Qt::CaseInsensitive))
        readFromRegistry();
    else
        readFromCurFolder();

    if (m_voice == "")
        initializeVoice();
    if (m_rate == "")
        initializeRate();

    if (m_settingsUpdater != nullptr)
    {
        m_settingsUpdater->updateVoice(m_voice);
        m_settingsUpdater->updateRate(m_rate);
    }
}

void Settings::readFromRegistry()
{
    QSettings settings("Emily", "Emily");
    m_voice = settings.value("Voice").toString();
    m_rate = settings.value("Rate").toString();

    //    This code helps to find the path of the settings file
    //    QString config_dir = QFileInfo(settings.fileName()).absolutePath() + "/";
    //    qDebug() << config_dir;
}

void Settings::readFromCurFolder()
{
    QString text;
    QString filename = QDir::currentPath() + "/userSettings";
    QFile file( filename );
    if(file.open(QIODevice::ReadOnly))
    {
        QTextStream in(&file);

        text = in.readAll();

        int i = text.indexOf(":");
        int l = text.indexOf("\r");
        m_voice = text.mid(i + 2, l - i - 2);
        text = text.right(l);
        i = text.indexOf(":");
        l = text.indexOf("\r");
        m_rate = text.mid(i + 2, l - i - 2);
    }

    file.close();
}

void Settings::writeSettings()
{
    QString dir =  QDir::currentPath();

    if (dir.startsWith("c", Qt::CaseInsensitive))
        writeToRegistry();
    else
        writeToCurFolder();
}

void Settings::writeToRegistry()
{
    QSettings settings("Emily", "Emily");
    settings.setValue("Voice", m_voice);
    settings.setValue("Rate", m_rate);
}

void Settings::writeToCurFolder()
{
    QString filename = QDir::currentPath() + "/userSettings";
    QFile file( filename );
    if ( file.open(QIODevice::ReadWrite | QIODevice::Text) )
    {
        file.resize(0);
        QTextStream stream( &file );
        stream << "Voice: " << m_voice << Qt::endl;
        stream << "Rate: " << m_rate;
    }
    file.close();
}

void Settings::setVoice(QString voice)
{
    m_voice = voice;
}

void Settings::setRate(QString rate)
{
    m_rate = rate.trimmed();
}

QString Settings::voice()
{
    return m_voice;
}

QString Settings::rate()
{
    return m_rate;
}

void Settings::initializeVoice()
{
    QString systemLanguage = QLocale::languageToString(QLocale::system().language());

    if (systemLanguage == QLocale::languageToString(QLocale::Greek))
        m_voice = herculesVoiceDisplay;
    else if (systemLanguage == QLocale::languageToString(QLocale::English))
    {
        QLocale locale = QLocale::system();
        if (locale.language() == QLocale::UnitedStates)
            m_voice = rmsVoiceDisplay;
        else
            m_voice = spikeVoiceDisplay;
    }
    else if (systemLanguage == QLocale::languageToString(QLocale::German))
        m_voice = pavoqueVoiceDisplay;
    else if (systemLanguage == QLocale::languageToString(QLocale::Turkish))
        m_voice = turkishVoiceDisplay;
    else if (systemLanguage == QLocale::languageToString(QLocale::French))
        m_voice = pierreVoiceDisplay;
    else if (systemLanguage == QLocale::languageToString(QLocale::Italian))
        m_voice = luciaVoiceDisplay;
    else if (systemLanguage == QLocale::languageToString(QLocale::Telugu))
        m_voice = teluguVoiceDisplay;
    else
        m_voice = herculesVoiceDisplay;
}

void Settings::initializeRate()
{
    m_rate = "50";
}

Settings::~Settings()
{

}
