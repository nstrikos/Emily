/*
 * Explanation: Emily addon receives indexes through 57118 port,
 * so I start QTcpServer listening at the 57118 port
 * and send an index when send() method is called.
*/

#include "nvdasender.h"

NvdaSender::NvdaSender(QObject *parent) : QObject(parent)
{
    nvdaIndexServer = new QTcpServer();
    connect(nvdaIndexServer, SIGNAL(newConnection()),
            this, SLOT(nvdaIndexServerAcceptConnection()));

    nvdaIndexServer->listen(QHostAddress::LocalHost, 57118);
    nvdaIndexServerConnection = nullptr;
}

void NvdaSender::nvdaIndexServerAcceptConnection()
{
    nvdaIndexServerConnection = nvdaIndexServer->nextPendingConnection();
    connect(nvdaIndexServerConnection, &QTcpSocket::errorOccurred, this, &NvdaSender::error);
    connect(nvdaIndexServerConnection, &QAbstractSocket::disconnected,
            nvdaIndexServerConnection, &QObject::deleteLater);
}

void NvdaSender::send(QString text)
{
    if (nvdaIndexServerConnection != nullptr)
    {
        QByteArray textToSend = text.toUtf8() ;
        nvdaIndexServerConnection->write(textToSend);
    }
}

void NvdaSender::error(QAbstractSocket::SocketError arg1)
{
    Q_UNUSED(arg1);
}

NvdaSender::~NvdaSender()
{
    nvdaIndexServer->close();
    delete nvdaIndexServer;
}

