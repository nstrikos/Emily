#ifndef COMMANDMEDIATOR_H
#define COMMANDMEDIATOR_H

#include <QObject>
#include "nvdacommandreceiveriface.h"
#include "settingsiface.h"
#include "settingsstorageiface.h"
#include "nvdacommandreceiver.h"
#include "settings.h"
#include "player.h"
#include "mainwindow.h"
#include "downloadmanager.h"
#include "settings.h"
#include "net/netClient.h"

class CommandMediator :
        public NvdaCommandReceiverIface,
        public SettingsIface,
        public SettingsStorageIface
{

public:
    CommandMediator(NvdaCommandReceiver &commandReceiver,
                    Player &player,
                    MainWindow &mainWindow,
                    DownloadManager &downloadManager,
                    Settings &settings,
                    NetClient &netClient);
    virtual ~CommandMediator();    

private:
    void receiveCommand(QString command) override;
    virtual void updateVoice(QString voice) override;
    virtual void updateRate(QString rate) override;
    virtual QString getVoice() override;
    virtual QString getRate() override;

    void handleVoiceCommand(QString command);
    void handleRateCommand(QString command);

    NvdaCommandReceiver &m_commandReceiver;
    Player &m_player;
    MainWindow &m_mainWindow;
    DownloadManager &m_downloadManager;
    Settings &m_settings;
    NetClient &m_netClient;
};

#endif // COMMANDMEDIATOR_H
