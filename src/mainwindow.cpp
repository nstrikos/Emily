#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("Emily"));
    ui->okButton->setFocus();

    m_storage = nullptr;

    createActions();
    createTrayIcon();
    initObjects();
    createShortcuts();
    createConnections();
    installAddon();
}

MainWindow::~MainWindow()
{
    if (chooseDiskDialog != nullptr)
        delete chooseDiskDialog;
    if (progressDialog != nullptr)
        delete progressDialog;

    delete trayIcon;
    delete trayIconMenu;
    delete minimizeAction;
    delete restoreAction;
    hotkeyThread->terminate();
    delete hotkeyThread;
    delete okShortcut;
    delete helpShortcut;
    delete installAddonShortcut;
    delete installDiskDriveShortcut;
    delete aboutShortcut;
    delete ui;
}

void MainWindow::createActions()
{
    minimizeAction = new QAction(tr("&Minimize"), this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(restore()));
}

void MainWindow::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);

    QIcon icon = QIcon(":/new/prefix1/resources/sound.png");
    trayIcon->setIcon(icon);
    setWindowIcon(icon);
    trayIcon->setToolTip(tr("Emily"));
    trayIcon->show();
}

void MainWindow::initObjects()
{
    chooseDiskDialog = nullptr;
    progressDialog = nullptr;
    hotkeyThread = new HotkeyThread();
    hotkeyThread->start();
    connect(hotkeyThread, SIGNAL(restoreWindow()), this, SLOT(restore()));
}

void MainWindow::createShortcuts()
{
    okShortcut = new QShortcut(QKeySequence("Esc"), this);
    connect(okShortcut, SIGNAL(activated()), this, SLOT(hide()));
    helpShortcut = new QShortcut(QKeySequence("F1"), this);
    connect(helpShortcut, SIGNAL(activated()), this, SLOT(help()));
    installDiskDriveShortcut = new QShortcut(QKeySequence("F2"), this);
    connect(installDiskDriveShortcut, SIGNAL(activated()), this, SLOT(installDiskDrive()));
    installAddonShortcut = new QShortcut(QKeySequence("F3"), this);
    connect(installAddonShortcut, SIGNAL(activated()), this, SLOT(installAddon()));
    aboutShortcut = new QShortcut(QKeySequence("F12"), this);
    connect(aboutShortcut, SIGNAL(activated()), this, SLOT(about()));
}

void MainWindow::createConnections()
{
    connect(ui->okButton, SIGNAL(clicked()), this, SLOT(hide()));
    connect(ui->helpButton, SIGNAL(clicked()), this, SLOT(help()));
    connect(ui->installDriversButton, SIGNAL(clicked()), this, SLOT(installAddon()));
    connect(ui->installDiskButton, SIGNAL(clicked()), this, SLOT(installDiskDrive()));
    connect(ui->aboutButton, SIGNAL(clicked()), this, SLOT(about()));
}

void MainWindow::installAddon()
{
    QString dir =  QDir::currentPath();

    if (dir.startsWith("c", Qt::CaseInsensitive)) {
        QString manifestFile, openmaryFile;
        QString nvdaRoamingPath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) +
                                  "\\AppData\\Roaming\\nvda";

        QDir nvdaDir(nvdaRoamingPath);
        if (nvdaDir.exists()) {

            QString emilyAddonPath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) +
                                     "\\AppData\\Roaming\\nvda\\addons\\Emily";

            QString emilyAddonSynthDriverPath = emilyAddonPath + "\\synthDrivers";
            QDir dir(emilyAddonSynthDriverPath);

            if(!dir.exists())
                dir.mkpath(emilyAddonSynthDriverPath);

            manifestFile = emilyAddonPath + "\\manifest.ini";
            openmaryFile = emilyAddonSynthDriverPath + "\\openmary.py";

            bool informUser = false;

            if (!QFile::exists(manifestFile) || !QFile::exists(openmaryFile))
                informUser = true;

            if (QFile::exists(manifestFile)) {
                QFile file(manifestFile);
                file.setPermissions(QFile::ReadOther | QFile::WriteOther);
                file.remove();
            }

            if (QFile::exists(openmaryFile)) {
                QFile file(openmaryFile);
                file.setPermissions(QFile::ReadOther | QFile::WriteOther);
                file.remove();
            }

            QFile::copy(":/new/prefix1/resources/manifest.ini", manifestFile);
            QFile::copy(":/new/prefix1/resources/openmary.py", openmaryFile);

            if (informUser) {
                QApplication::beep();
                QMessageBox msgBox;
                msgBox.setText(tr("Addon installation is complete"));
                msgBox.setIcon( QMessageBox::Information );
                msgBox.exec();
            }
        }
    }
}

void MainWindow::setStorage(SettingsStorageIface *storage)
{
    m_storage = storage;
}

void MainWindow::setVisible(bool visible)
{
    minimizeAction->setEnabled(visible);
    restoreAction->setEnabled(isMaximized() || !visible);
    QMainWindow::setVisible(visible);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (trayIcon->isVisible())
    {
        hide();
        event->ignore();
    }
}

void MainWindow::quitReceived()
{
    qApp->quit();
}

void MainWindow::restore()
{
    this->showNormal();
    this->raise();
    this->activateWindow();
    this->setFocus();
    ui->okButton->setFocus();
}

void MainWindow::help()
{
    QString directory = QDir::currentPath();
    QString helpFile = directory += "/resources/help.txt";
    QString resource = "file:///" + helpFile;
    QDesktopServices::openUrl(QUrl(resource, QUrl::TolerantMode));
}

void MainWindow::about()
{
    QMessageBox msgBox;
    msgBox.setText("Emily 0.9\nEmail : nstrikos@yahoo.gr\nIcons: Linecons Free by Designmodo\nLicense: Creative Commons");
    msgBox.setIcon( QMessageBox::Information );
    msgBox.exec();
}

void MainWindow::installDiskDrive()
{
    if (!chooseDiskDialog)
        chooseDiskDialog = new ChooseDiskDialog(this);

    if (chooseDiskDialog->searchDrivesAndAddtoCombobox())
    {
        if (chooseDiskDialog->exec())
        {
            QString drivePath = chooseDiskDialog->getDrivePath();
            if (drivePath != "")
            {
                QDir installationDir = drivePath + "/Emily";
                if (!installationDir.exists())
                    installationDir.mkdir(".");

                //Create dialog before connecting signals from the thread
                if (!progressDialog)
                    progressDialog = new ProgressDialog(this);

                //Set up thread
                QThread* copyThread = new QThread;
                Copy* copy = new Copy(drivePath, m_storage->getVoice(), m_storage->getRate());
                int maxFiles = copy->countMaxFiles();
                copy->moveToThread(copyThread);
                connect(copyThread, SIGNAL(started()), copy, SLOT(process()));
                connect(copy, SIGNAL(finished()), copyThread, SLOT(quit()));

                //We schedule the objects for deletion, no leak memory here
                connect(copy, SIGNAL(finished()), copy, SLOT(deleteLater()));
                connect(copyThread, SIGNAL(finished()), copyThread, SLOT(deleteLater()));
                connect(copy, SIGNAL(increase()), progressDialog, SLOT(increase()));
                connect(copy, SIGNAL(finished()), this, SLOT(installationComplete()));

                //Activate progress dialog
                progressDialog->resetProgressBar(maxFiles);
                progressDialog->show();
                progressDialog->raise();
                copyThread->start();
            }
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText(tr("There are no disk drives to install"));
        msgBox.setIcon( QMessageBox::Information );
        msgBox.exec();
    }
}

void MainWindow::installationComplete()
{
    progressDialog->setCanClose(true);
    progressDialog->close();
    if (progressDialog != nullptr)
    {
        delete progressDialog;
        progressDialog = nullptr;
    }

    QMessageBox msgBox;
    msgBox.setText(tr("Installation completed"));
    msgBox.setIcon( QMessageBox::Information );
    msgBox.exec();
}

//void MainWindow::startNVDA()
//{
//    This is not necessary because NVDA addon waits for the server to come up
//    INPUT ip;
//    ip.type = INPUT_KEYBOARD;
//    ip.ki.wScan = 0;
//    ip.ki.time = 0;
//    ip.ki.dwExtraInfo = 0;


//    // Press the "Ctrl" key
//    ip.ki.wVk = VK_CONTROL;
//    ip.ki.dwFlags = 0; // 0 for key press
//    SendInput(1, &ip, sizeof(INPUT));

//    // Press the "ALT" key
//    ip.ki.wVk = VK_MENU;
//    ip.ki.dwFlags = 0; // 0 for key press
//    SendInput(1, &ip, sizeof(INPUT));

//    // Press the "N" key
//    ip.ki.wVk = 'N';
//    ip.ki.dwFlags = 0; // 0 for key press
//    SendInput(1, &ip, sizeof(INPUT));

//    // Release the "N" key
//    ip.ki.wVk = 'N';
//    ip.ki.dwFlags = KEYEVENTF_KEYUP;
//    SendInput(1, &ip, sizeof(INPUT));

//    // Release the "ALT" key
//    ip.ki.wVk = VK_MENU;
//    ip.ki.dwFlags = KEYEVENTF_KEYUP;
//    SendInput(1, &ip, sizeof(INPUT));

//    // Release the "Ctrl" key
//    ip.ki.wVk = VK_CONTROL;
//    ip.ki.dwFlags = KEYEVENTF_KEYUP;
//    SendInput(1, &ip, sizeof(INPUT));
//}

////    This code helps to find the path of the settings file
////    QString config_dir = QFileInfo(settings.fileName()).absolutePath() + "/";
////    qDebug() << config_dir;
