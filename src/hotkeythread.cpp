#include "hotkeythread.h"
#include "windows.h"
#include <QApplication>
#define MOD_NOREPEAT    0x4000
#define MOD_ALT         0x0001
#define MOD_CONTROL     0x0002

HotkeyThread::HotkeyThread()
{

}

void HotkeyThread::run()
{
    RegisterHotKey(nullptr,3,MOD_ALT | MOD_CONTROL | MOD_NOREPEAT,0x45); //Ctrl + Alt + E

    QApplication::processEvents();

    MSG msg;
    while(GetMessage(&msg,nullptr,0,0)){
        TranslateMessage(&msg);
        DispatchMessage(&msg);
        if (msg.message == WM_HOTKEY){
            if (msg.wParam == 3)
                emit restoreWindow();
        }
    }
}
