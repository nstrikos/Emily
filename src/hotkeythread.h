#ifndef HOTKEYTHREAD_H
#define HOTKEYTHREAD_H

#include <QThread>

class HotkeyThread : public QThread
 {
     Q_OBJECT
public:
    HotkeyThread();

protected:
    void run();

signals:
     void restoreWindow();
};

#endif // HOTKEYTHREAD_H
