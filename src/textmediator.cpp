/*
 * Explanation: This class implements the methods of textmediator.h
 * It follows the mediator design pattern. Reading "Designing Object Oriented
 * C++ Applications Using The Booch Method" p. 71 by Robert Martin and
 * "C++ All-in-One For Dummies" p. 356 by John Paul Mueller and‎ Jeff Cogswell
 * lead to this design.
*/

#include "textmediator.h"

TextMediator::TextMediator(NvdaTextReceiver &textReceiver,
                           NvdaTextHandler &textHandler,
                           DownloadManager &downloadManager,
                           Player &player,
                           NvdaSender &nvdaSender,
                           NetClient &netClient) :
    m_textReceiver(textReceiver),
    m_textHandler(textHandler),
    m_downloadManager(downloadManager),
    m_player(player),
    m_nvdaSender(nvdaSender),
    m_netClient(netClient)
{
    m_netClient.setClientIface(this); //This is better to go first, to avoid pointer error
    m_textReceiver.setReceiver(this);
    m_textHandler.setReceiver(this);
    m_downloadManager.setWavReceiver(this);
    m_player.setIndexHandler(this);
}

void TextMediator::handleRawText(QString rawText)
{
    m_textHandler.handleText(rawText);
}

void TextMediator::handleNormalizedText(QString text, QString index)
{
    if (text != "")
        m_downloadManager.textToSpeech(text, index);
        //m_netClient.sendTextToSpeech(text, index);
    else
        m_nvdaSender.send(index);
}

void TextMediator::handleWav(QIODevice *buffer, QString index, QString filename)
{
    m_player.addPlaylist(buffer, index, filename);
}

void TextMediator::sendIndex(QString index)
{
    m_nvdaSender.send(index);
}

void TextMediator::newIndex(const QString &index)
{
    m_nvdaSender.send(index);
}

TextMediator::~TextMediator()
{

}
