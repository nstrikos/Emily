#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>

const QString emilyVoice = "LOCALE=el&VOICE=emily-v2.0.1-hmm";
const QString herculesVoice = "LOCALE=el&VOICE=alchemist-1992-hsmm-hsmm";
const QString googleVoice = "LOCALE=el&VOICE=google-10";
const QString spikeVoice = "LOCALE=en_US&VOICE=dfki-spike-hsmm";
const QString prudenceVoice = "LOCALE=en_US&VOICE=dfki-prudence-hsmm";
const QString poppyVoice = "LOCALE=en_US&VOICE=dfki-poppy-hsmm";
const QString obadiahVoice = "LOCALE=en_US&VOICE=dfki-obadiah-hsmm";
const QString rmsVoice = "LOCALE=en_US&VOICE=cmu-rms-hsmm";
const QString bdlVoice = "LOCALE=en_US&VOICE=cmu-bdl-hsmm";
const QString pavoqueVoice = "LOCALE=de&VOICE=dfki-pavoque-neutral-hsmm";
const QString pierreVoice = "LOCALE=fr&VOICE=upmc-pierre-hsmm";
const QString luciaVoice = "LOCALE=it&VOICE=istc-lucia-hsmm";
const QString turkishVoice = "LOCALE=tr&VOICE=dfki-ot-hsmm";
const QString teluguVoice = "LOCALE=te&VOICE=cmu-nk-hsmm";

const QString emilyVoiceDisplay = "Emily - Greek";
const QString herculesVoiceDisplay = "Hercules - Greek";
const QString spikeVoiceDisplay = "Spike - English";
const QString prudenceVoiceDisplay = "Prudence - English";
const QString poppyVoiceDisplay = "Poppy - English";
const QString obadiahVoiceDisplay = "Obadiah - English";
const QString rmsVoiceDisplay = "Rms - US English";
const QString bdlVoiceDisplay = "Bdl - US English";
const QString pavoqueVoiceDisplay = "Pavoque - German";
const QString pierreVoiceDisplay = "Pierre - French";
const QString luciaVoiceDisplay = "Lucia - Italian";
const QString turkishVoiceDisplay = "Ot - Turkish";
const QString teluguVoiceDisplay = "Nk - Telugu";

const QString nvdaIndex = "(NVDA Index)";

#endif // CONSTANTS_H
