/*
 * Every class that needs to receive wav files from a DownloadManager
 * must implement this interface
 * DownloadManager receiver will call this function whenever it receives
 * a wav file
*/

#ifndef DOWNLOADMANAGERIFACE_H
#define DOWNLOADMANAGERIFACE_H

class QIODevice;
class QString;

class DownloadManagerIface
{
public:
    virtual void handleWav(QIODevice* buffer, QString index, QString filename) = 0;
};

#endif // DOWNLOADMANAGERIFACE_H
