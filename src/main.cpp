#include <QTranslator>

#include "maryserver.h"
#include "nvdatextreceiver.h"
#include "nvdatexthandler.h"
#include "nvdasender.h"
#include "downloadmanager.h"
#include "player.h"
#include "textmediator.h"
#include "nvdacommandreceiver.h"
#include "mainwindow.h"
#include "settings.h"
#include "commandmediator.h"
#include "net/netClient.h"

#ifndef QT_NO_SYSTEMTRAYICON

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);

    QTranslator appTranslator;
    if (appTranslator.load("emily_" + QLocale::system().name(), "./translations"))
        app.installTranslator(&appTranslator);

    if (!QSystemTrayIcon::isSystemTrayAvailable())
    {
        QMessageBox::critical(0, QObject::tr("Systray"),
                              QObject::tr("Application could not find system tray"));
        return 1;
    }

    QApplication::setQuitOnLastWindowClosed(false);

    //---- Check for another instance code snippet ----
    // key must be unique for the application
    QSharedMemory sharedMemory;
    sharedMemory.setKey("Emily - Open Mary voices for NVDA");
    if(sharedMemory.attach())
    {
        ;
        //Do nothing;
    }

    if (!sharedMemory.create(1))
    {
        QMessageBox msgBox;
        msgBox.setText( QObject::tr("Application is already running") );
        msgBox.setIcon( QMessageBox::Critical );
        msgBox.exec();
        qWarning() << "Can't start more than one instance of the application.";

        exit(0);
    }
    //---- END OF Check for another instance code snippet ----

    MaryServer *maryServer;
    NvdaTextReceiver *nvdaTextReceiver;
    NvdaTextHandler *nvdaTextHandler;
    DownloadManager *downloadManager;
    Player *player;
    NvdaSender *nvdaSender;
    MainWindow *mainWindow;
    Settings *settings;
    NvdaCommandReceiver *nvdaCommandReceiver;
    TextMediator *textMediator;
    CommandMediator *commandMediator;

    NetClient *netClient;


    maryServer = new MaryServer();
    maryServer->start();
    nvdaTextReceiver = new NvdaTextReceiver();
    nvdaTextHandler = new NvdaTextHandler();
    downloadManager = new DownloadManager();
    player = new Player();
    nvdaSender = new NvdaSender();
    netClient = new NetClient();

    textMediator = new TextMediator(*nvdaTextReceiver,
                                    *nvdaTextHandler,
                                    *downloadManager,
                                    *player,
                                    *nvdaSender,
                                    *netClient);

    nvdaCommandReceiver = new NvdaCommandReceiver();
    mainWindow = new MainWindow();
    settings = new Settings();

    commandMediator = new CommandMediator(*nvdaCommandReceiver,
                                          *player,
                                          *mainWindow,
                                          *downloadManager,
                                          *settings,
                                          *netClient);

    int i = app.exec();

    delete settings;
    delete nvdaCommandReceiver;
    delete commandMediator;
    delete nvdaTextHandler;
    delete downloadManager;
    delete player;
    delete nvdaTextReceiver;
    delete textMediator;
    delete nvdaSender;
    delete mainWindow;
    delete maryServer;
    delete netClient;

    return  i;
}

#else

#include <QLabel>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QString text(tr("Το QSystemTrayIcon δεν υποστηρίζεται σε αυτό το λειτουργικό σύστημα"));

    QLabel *label = new QLabel(text);
    label->setWordWrap(true);
    label->show();
    app.exec();
}

#endif
