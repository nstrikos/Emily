/*
 * Class : Player
 * Role : Player is responsible for playing media buffers.
 * When a buffer is played, player will send its index to nvda
 * Call playFile function to add a buffer media to the list of the player.
 * Player is responsible for deleting the buffers and freeing memory
 * Players also controls the rate of the mediaplayer, and can stop, pause and restart
 * the media player
 * Collaborators : When a wav file is played its index is send to m_indexHandler
 * which probably is a textMediator object
 */

#ifndef PLAYER_H
#define PLAYER_H

#include <QBuffer>
#include <QMediaPlayer>
#include <QAudioOutput>
#include "playeriface.h"

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
#include <QAudioOutput>
#endif

class Player : public QObject
{
    Q_OBJECT

public:
    Player();
    ~Player();
    void setIndexHandler(PlayerIface *indexHandler);
    void addPlaylist(QIODevice* buffer, QString index, QString filename);
    void stop();
    void resume();
    void pause();

private slots:
    void play();

private:
    void sendIndexToNVDA();
    void clearPlayedBuffers();
    void clearCreatedBuffers();
    QMediaPlayer *m_mediaPlayer;

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    QAudioOutput *m_audioOutput;
#endif

    QVector<QIODevice*> m_createdBuffers;
    QVector<QIODevice*> m_playedBuffers;
    QIODevice *m_buffer;
    QStringList m_createdIndexes;
    QStringList m_filenames;
    QStringList m_playedFilenames;

    PlayerIface *m_indexHandler;
};

#endif // PLAYER_H
