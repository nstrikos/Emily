/*
 * Explanation: Emily addon sends text through 57116 port,
 * so I start QTcpServer listening at the 57116 port
 * When there is a readyRead() signal I call getText() and send text to m_receiver.
 * When there is an error I call error() and display the error
*/

#include "nvdatextreceiver.h"

NvdaTextReceiver::NvdaTextReceiver()
{
    m_receiver = nullptr;
    nvdaTextServer = new QTcpServer();
    connect(nvdaTextServer, SIGNAL(newConnection()),
            this, SLOT(nvdaTextServerAcceptConnection()));

    nvdaTextServer->listen(QHostAddress::LocalHost, 57116);
    nvdaTextServerConnection = nullptr;
}

void NvdaTextReceiver::setReceiver(NvdaTextReceiverIface *receiver)
{
    m_receiver = receiver;
}

void NvdaTextReceiver::nvdaTextServerAcceptConnection()
{
    nvdaTextServerConnection = nvdaTextServer->nextPendingConnection();
    connect(nvdaTextServerConnection, &QTcpSocket::readyRead, this, &NvdaTextReceiver::getText);
    connect(nvdaTextServerConnection, &QTcpSocket::errorOccurred, this, &NvdaTextReceiver::error),
    connect(nvdaTextServerConnection, &QAbstractSocket::disconnected,
            nvdaTextServerConnection, &QObject::deleteLater);
}

void NvdaTextReceiver::getText()
{
    QString text(nvdaTextServerConnection->readAll());
    if (m_receiver != nullptr)
        m_receiver->handleRawText(text);
}

void NvdaTextReceiver::error(QAbstractSocket::SocketError arg1)
{
    Q_UNUSED(arg1);
}

NvdaTextReceiver::~NvdaTextReceiver()
{
    nvdaTextServer->close();
    delete nvdaTextServer;
}
