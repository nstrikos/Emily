/*
 * Explanation: Reading the comments from top to the bottom, will give you
 * idea of how the text is converted to wav.
 * This class should be split into more classes. But splitting needs very detailed design
 * to avoid bugs and memory leaks, so I'm leaving it the way it is.
*/

#include "downloadmanager.h"
#include "constants.h"
#include <QBuffer>
#include <QFile>
#include <QDir>
#include <QApplication>

DownloadManager::DownloadManager()
{
    m_wavReceiver = nullptr;
    m_reply = nullptr;
    m_file = nullptr;
    m_downloading = false;
    m_voice = herculesVoice;
    m_rate = 1.0;
    m_isWav = false;
    m_soxFile = nullptr;
    m_file = nullptr;
    m_counter = 0;

    m_process = new QProcess();

    QDir dir(QDir::tempPath()+"/emily/");
    if (!dir.exists()){
        dir.mkdir(".");
    }
}

void DownloadManager::setWavReceiver(DownloadManagerIface *wavReceiver)
{
    m_wavReceiver = wavReceiver;
}

void DownloadManager::textToSpeech(QString text, QString index)
{
    //This is the entry point to the algorithm
    //When new text with a new index is coming,
    //we add them to m_textList and m_indexList
    //and we call processLists()
    m_textList << text;
    m_indexList << index;

    processLists();
}

void DownloadManager::processLists()
{
    //This function is called when new text arrives
    //or when the processing of a text has finished

    //If no text is being processed
    //we take the first text and index and we process them

    //If text is being process, this function will get called
    //when the processing is finished

    if (!m_downloading) {
        if (!m_textList.isEmpty()) {
            QString text = m_textList.takeFirst();
            QString index = m_indexList.takeFirst();
            performTextToWav(text, index);
        }
    }
}

void DownloadManager::performTextToWav(QString text, QString index)
{
    //we proceed only if text is not being processed
    if (!m_downloading) {
        //we inform the object that text is being processed
        //so incoming texts will have to wait
        m_downloading = true;

        //we store the current index, so we will be able to send to nvda
        m_index = index;

        //we create a new buffer to write wav
        if (m_rate == 1) {
            m_isWav = false;
            m_file = new QBuffer();
        } else {
            //or we create a new file for sox
            m_isWav = true;
            m_counter++;
            m_filename = QDir::tempPath() + "/emily/emily-" + QString::number(m_counter) + ".wav";
            m_soxFilename = QDir::tempPath() + "/emily/emily-" + QString::number(m_counter) + "-sox.wav";
            m_file = new QFile(m_filename);
            m_soxFile = new QFile(m_soxFilename);
        }

        //we make sure buffer is writable
        if (!m_file->open(QIODevice::WriteOnly)) {
            delete m_file;
            m_file = nullptr;
            return;
        }

        //We start writing the command we will send to http server
        QString part1 = "http://localhost:59125/process?INPUT_TEXT=";
        QString part2 = "&INPUT_TYPE=TEXT&OUTPUT_TYPE=AUDIO&AUDIO=AU_FILE&";
        QString part3 = m_voice;

        QString command = part1 + text + part2 + part3;

        //Let the object know that we have not cancelled the request
        m_httpRequestAborted = false;

        //schedule the request
        startRequest(command);
    }
}

void DownloadManager::startRequest(QUrl url)
{
    //Send the request to http server
    m_reply = m_qnam.get(QNetworkRequest(url));

    //When the http server has new data, httpReadyRead() gets called
    connect(m_reply, SIGNAL(readyRead()), this, SLOT(httpReadyRead()));

    //When the request has finished, httpFinished() gets called
    connect(m_reply, SIGNAL(finished()),  this, SLOT(httpFinished()));
}

void DownloadManager::httpReadyRead()
{
    // this slot gets called every time the QNetworkReply has new data.
    // We read all of its new data and write it into the file.
    // That way we use less RAM than when reading it at the finished()
    // signal of the QNetworkReply
    if (m_file)
        m_file->write(m_reply->readAll());
}

void DownloadManager::httpFinished()
{
    //When the server has sent all data
    //we delete m_reply
    if (m_reply != nullptr) {
        m_reply->deleteLater();
        m_reply = nullptr;
    }
    if (m_file != nullptr)
        m_file->close();

    //If the process was cancelled, we stop here
    if (m_httpRequestAborted)
        return;

    //otherwise we call finishRequest()
    finishRequest();
}

void DownloadManager::finishRequest()
{
    //When the request is over
    //we let the object know that we dont download anymore
    if (m_isWav == false) {
        m_downloading = false;

        //And we send the m_file buffer and m_index to the receiver
        if (m_wavReceiver != nullptr)
            m_wavReceiver->handleWav(m_file, m_index, "");

        //Finally we call processLists to start over
        processLists();
    } else {
        //we have to send the file to a process
        QString directory = QDir::currentPath();
        directory.replace("/", "\\");
        QString part1 = "\"" + directory + "\\sox\\sox\"";
        QString part2 = QString::number(m_rate);
        QString command = part1 + " " + m_filename + " " + m_soxFilename + " tempo " + part2;

        connect(m_process, SIGNAL(finished(int)), this, SLOT(processFinished()));

        if (m_process->processId() == 0)
            m_process->start(command);
    }
}

void DownloadManager::processFinished()
{
    disconnect(m_process, SIGNAL(finished(int)), this, SLOT(processFinished()));

    if (QFile::exists(m_filename))
        QFile::remove(m_filename);

    if (m_file != nullptr) {
        delete m_file;
        m_file = nullptr;
    }
    m_downloading = false;

    if (m_wavReceiver != nullptr)
        m_wavReceiver->handleWav(m_soxFile, m_index, m_soxFilename);

    //Finally we call processLists to start over
    processLists();
}

void DownloadManager::cancelDownload()
{
    //When we need to cancel the process
    //First we clear the lists, so the object has no more text to process
    clearLists();

    //And then we send cancel to the http server
    if (m_reply != nullptr) {
        m_httpRequestAborted = true;
        m_reply->abort();
        m_reply = nullptr;
    }

    //The rest is very important to avoid memory leak
    //Take into account all possible states of the class
    if (m_process->processId() != 0) {
        disconnect(m_process, SIGNAL(finished(int)), this, SLOT(processFinished()));
        m_process->terminate();
    }

    if (m_downloading || m_process->processId() != 0) {
        if (m_file != nullptr) {
            m_file->close();
            if (QFile::exists(m_filename))
                if (!QFile::remove(m_filename)) {
                    m_deleteFilenames << m_filename;
                }
            delete m_file;
            m_file = nullptr;
        }
        if (m_soxFile != nullptr) {
            m_soxFile->close();
            if (QFile::exists(m_soxFilename))
                if (!QFile::remove(m_soxFilename)) {
                    m_deleteFilenames << m_soxFilename;
                }

            delete m_soxFile;
            m_soxFile = nullptr;
        }
    }

    deleteFiles();

    if (m_downloading)
        m_downloading = false;
}

void DownloadManager::clearLists()
{
    m_textList.clear();
    m_indexList.clear();
}

void DownloadManager::deleteFiles()
{
    for (int i = 0; i < m_deleteFilenames.size(); i++) {
        if (QFile::exists(m_deleteFilenames.at(i))) {
            if (QFile::remove(m_deleteFilenames.at(i)))
                m_deleteFilenames.removeAt(i);
        } else {
            m_deleteFilenames.removeAt(i);
        }
    }
}

void DownloadManager::setVoice(QString voice)
{
    if (voice == herculesVoiceDisplay)
        m_voice = herculesVoice;
    else if (voice == emilyVoiceDisplay)
        m_voice = emilyVoice;
    else if (voice == spikeVoiceDisplay)
        m_voice = spikeVoice;
    else if (voice == rmsVoiceDisplay)
        m_voice = rmsVoice;
    else if (voice == prudenceVoiceDisplay)
        m_voice = prudenceVoice;
    else if (voice == poppyVoiceDisplay)
        m_voice = poppyVoice;
    else if (voice == obadiahVoiceDisplay)
        m_voice = obadiahVoice;
    else if (voice == bdlVoiceDisplay)
        m_voice = bdlVoice;
    else if (voice == pavoqueVoiceDisplay)
        m_voice = pavoqueVoice;
    else if (voice == pierreVoiceDisplay)
        m_voice = pierreVoice;
    else if (voice == luciaVoiceDisplay)
        m_voice = luciaVoice;
    else if (voice == turkishVoiceDisplay)
        m_voice = turkishVoice;
    else if (voice == teluguVoiceDisplay)
        m_voice = teluguVoice;
    else
        m_voice = herculesVoice;
}

void DownloadManager::setRate(QString rateString)
{
    float rate = rateString.toFloat();
    m_rate = (0.01 * rate) + 0.5;
}

DownloadManager::~DownloadManager()
{
    QDir dir(QDir::tempPath()+"/emily/");
    dir.removeRecursively();

    delete m_process;
}
