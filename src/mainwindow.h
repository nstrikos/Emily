#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "choosediskdialog.h"
#include "copythread.h"
#include "progressdialog.h"
#include "constants.h"
#include <QtWidgets>
#include <QTimer>
#include "hotkeythread.h"
#include "settingsstorageiface.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setStorage(SettingsStorageIface *storage);
    void setVisible(bool visible);

protected:
    void closeEvent(QCloseEvent *event);

public slots:
    void quitReceived();

signals:
    void writeSettings();

private slots:
    void restore();
     void installAddon();
    void help();
    void about();
    void installDiskDrive();
    void installationComplete();

private:
    Ui::MainWindow *ui;

    void createActions();
    void createTrayIcon();
    void initObjects();
    void createShortcuts();
    void createConnections();
    double getUsedMemory();

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;

    QAction *minimizeAction;
    QAction *restoreAction;

    ChooseDiskDialog *chooseDiskDialog;
    ProgressDialog *progressDialog;
    HotkeyThread *hotkeyThread;

    QShortcut *okShortcut;
    QShortcut *helpShortcut;
    QShortcut *installAddonShortcut;
    QShortcut *installDiskDriveShortcut;
    QShortcut *aboutShortcut;

    SettingsStorageIface *m_storage;
};

#endif // MAINWINDOW_H
