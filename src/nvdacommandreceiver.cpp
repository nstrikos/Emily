/*
 * Explanation: Emily addon sends commands through 57117 port,
 * so I start QTcpServer listening at the 57117 port
 * When there is a readyRead() signal I call getCommand()
 * and send text to m_commandReceiver.
 * When there is an error I call error() and display the error
*/

#include "nvdacommandreceiver.h"

NvdaCommandReceiver::NvdaCommandReceiver()
{
    m_commandReceiver = nullptr;
    nvdaCommandServer = new QTcpServer();
    connect(nvdaCommandServer, SIGNAL(newConnection()),
            this, SLOT(nvdaCommandServerAcceptConnection()));

    nvdaCommandServer->listen(QHostAddress::LocalHost, 57117);

}

void NvdaCommandReceiver::setReceiver(NvdaCommandReceiverIface *receiver)
{
    m_commandReceiver = receiver;
}

void NvdaCommandReceiver::nvdaCommandServerAcceptConnection()
{
    nvdaCommandServerConnection = nvdaCommandServer->nextPendingConnection();
    connect(nvdaCommandServerConnection, &QTcpSocket::readyRead, this, &NvdaCommandReceiver::getCommand);
    connect(nvdaCommandServerConnection, &QTcpSocket::errorOccurred, this, &NvdaCommandReceiver::error);
    connect(nvdaCommandServerConnection, &QAbstractSocket::disconnected,
            nvdaCommandServerConnection, &QObject::deleteLater);
}

void NvdaCommandReceiver::getCommand()
{
    QString command(nvdaCommandServerConnection->readAll());
    if (m_commandReceiver != nullptr)
        m_commandReceiver->receiveCommand(command);
}

void NvdaCommandReceiver::error(QAbstractSocket::SocketError arg1)
{
    Q_UNUSED(arg1);
}

NvdaCommandReceiver::~NvdaCommandReceiver()
{
    nvdaCommandServer->close();
    delete nvdaCommandServer;
}
