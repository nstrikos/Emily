/*
 * Class : DownloadManager
 * Role : My responsibility is to convert text to speech,
 * call textToSpeech to convert text to wav
 * Collaborator : When a wav file is created I send it to m_wavReceiver
 * which probably is a textMediator object
*/

#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QtNetwork/QNetworkReply>
#include <QProcess>
#include "downloadmanageriface.h"

class DownloadManager : public QObject
{
    Q_OBJECT
public:
    DownloadManager();
    ~DownloadManager();
    void setWavReceiver(DownloadManagerIface *wavReceiver);
    void setVoice(QString voice);
    void textToSpeech(QString text, QString index);
    void cancelDownload();
    void setRate(QString rateString);

private slots:
    void httpFinished();
    void httpReadyRead();
    void processFinished();

private:
    void processLists();
    void performTextToWav(QString text, QString index);
    void startRequest(QUrl url);
    void finishRequest();
    void clearLists();
    void deleteFiles();

    QIODevice *m_file;
    QIODevice *m_soxFile;
    QNetworkAccessManager m_qnam;
    QNetworkReply *m_reply;
    bool m_httpRequestAborted;
    bool m_downloading;
    QStringList m_textList;
    QStringList m_indexList;
    QStringList m_deleteFilenames;
    QString m_index;
    QString m_voice;
    float m_rate;
    bool m_isWav;
    QString m_filename;
    QString m_soxFilename;
    QProcess *m_process;
    qint64 m_counter;

    DownloadManagerIface *m_wavReceiver;
};

#endif // DOWNLOADMANAGER_H
