import synthDriverHandler
from synthDriverHandler import (SynthDriver, synthIndexReached, synthDoneSpeaking)
from collections import OrderedDict
import os.path

#from logHandler import log
#use with log.info(text)

from speech.commands import (
    IndexCommand,
	CharacterModeCommand,
	LangChangeCommand,
	BreakCommand,
	PitchCommand,
	RateCommand,
	VolumeCommand,
	PhonemeCommand,
)

import threading
import string
import socket
import subprocess

def startProcess():
    cwd = os.getcwd()
    drive = cwd[:1]
    if drive == 'c' or drive == 'C':
        filepath = "c:/Program Files/Emily/emily.exe"
        if os.path.isfile(filepath):
            p = subprocess.Popen("c:/Program Files/Emily/emily.exe", cwd="c:/Program Files/Emily")
        else:
            p = subprocess.Popen("c:/Program Files (x86)/Emily/emily.exe", cwd="c:/Program Files (x86)/Emily")
    else:
        filepath = str(drive) + ":/Emily/emily.exe"
        dir = str(drive) + ":/Emily"
        p = subprocess.Popen(filepath, cwd=dir)

class SynthDriver(SynthDriver):

    supportedSettings=(
        SynthDriver.VoiceSetting(),
        SynthDriver.VariantSetting(),
        SynthDriver.RateSetting(),
        SynthDriver.RateBoostSetting(),
        SynthDriver.PitchSetting(),
        SynthDriver.InflectionSetting(),
        SynthDriver.VolumeSetting(),
    )
	
    supportedCommands = {
        IndexCommand,
        CharacterModeCommand,
        LangChangeCommand,
        BreakCommand,
        PitchCommand,
        RateCommand,
        VolumeCommand,
        PhonemeCommand,
    }
    
    supportedNotifications = {synthIndexReached, synthDoneSpeaking}
    name = "openmary"
    description = "Emily - Open Mary"
    supportedSettings = (SynthDriver.RateSetting(), SynthDriver.VoiceSetting())

    #subclass thread to read index at port 57118
    class indexThread (threading.Thread):
        def __init__(self, indexList, synth):
            threading.Thread.__init__(self)
            self.indexList = indexList
            self.timeToDie = False
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.settimeout(10000)
            self.host = 'localhost';
            self.port = 57118;
            self.synth = synth
	 
			
            try:
                self.socket.connect((self.host , self.port))
            except socket.error as ex:
                totalDelay = 0.0
                delay = 0.25
                while totalDelay < 250000000.0:
                    try:
                        self.socket.connect((self.host, self.port))
                        return
                    except socket.error as ex:
                        time.sleep(delay)
                        totalDelay = totalDelay + delay
                raise IOError

        def run(self):
            while not self.timeToDie:
                reply = self.socket.recv(4)
                if len(reply) > 0:
                    if reply != "":
                        rcv = int(float(reply))
                        if rcv == -1:
                            self.synth.doneReading()
                        else:
                            self.indexList[0] = int(float(reply))
                            self.synth.indexChanged()
                        
        def quit(self):
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()
            self.timeToDie = True

    #class to send text at port 57116
    class textSender:
        def __init__(self):
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.settimeout(None)
            self.socket = socket.create_connection(('localhost', 57116))

        def send(self, data):
            self.socket.sendall(data.encode('utf-8'))

        def quit(self):
            self.sock.close()

    #class to send command at port 57117
    class commandSender():
        def __init__(self):
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.settimeout(None)
            self.socket = socket.create_connection(('localhost', 57117))

        def send(self, data):
            self.socket.sendall(data.encode('utf-8'))

        def quit(self):
            self.sock.close()

    def __init__(self):
        startProcess()
	
        self.myVoice = 'hercules - greek'
        self.voices = ('hercules - greek',
                        'emily',
                        'spike',
                        'rms',
                        'obadiah',
                        'prudence',
                        'poppy',
                        'bdl',
                        'pavoque - german',
                        'ot - turkish',
                        'lucia - italian',
                        'pierre - french',
                        'nk - telugu')

        self.indexList = [0]
        self.indexThread = SynthDriver.indexThread(self.indexList, self)
        self.indexThread.timeToDie = False
        self.indexThread.start()
        self.textSender = SynthDriver.textSender()
        self.commandSender = SynthDriver.commandSender()
        self.myRate = 50
        self.myPitch = 1
    
    @classmethod
    def check(cls):
        return True

    def speak(self, speechSequence):
        textList = []
        for item in speechSequence:
            if isinstance(item,str):
                textList.append(item)
            elif isinstance(item,IndexCommand):
                textList.append("(NVDA Index)%d#"%item.index)
                    
        text=u"".join(textList)
        self.textSender.send(text)

    def _get_voice(self):
        return self.myVoice

    def _getAvailableVoices(self):
        o = OrderedDict()
        for v in self.voices:
            o[v] = synthDriverHandler.VoiceInfo(v, v)
            if o[v] == None:
                o[v] = 'hercules - greek'
        return o

    def cancel(self):
        self.commandSender.send("Cancel")

    def pause(self, switch):
        if switch:
            self.commandSender.send("Pause")
        else:
            self.commandSender.send("Start")
            
    def doneReading(self):
        synthDoneSpeaking.notify(synth=self)
        
    def indexChanged(self):
        synthIndexReached.notify(synth=self, index=self.indexList[0])
        
    def _set_rate(self, rate):
        self.myRate = rate
        self.commandSender.send("Rate " + str(self.myRate))

    def _get_rate(self):
        return self.myRate

    def _set_voice(self, value):
        if value not in self.voices:
            value = 'hercules - greek'
        if (self.myVoice == 'None'):
            self.myVoice = 'hercules - greek'
            value = 'hercules - greek'
        if (self.myVoice != value):
            self.myVoice = value
            self.commandSender.send("Voice " + str(self.myVoice))

    def terminate(self):
        self.commandSender.send("Quit")
        self.indexThread.quit()
        self.indexThread.join()
        self.stopServer()


    def stopServer(self):
        try:
            self.textSender.socket.shutdown(socket.SHUT_RDWR)
            self.commandSender.socket.shutdown(socket.SHUT_RDWR)
        except socket.error:
            pass
        self.textSender.socket.close()
        self.commandSender.socket.close()
