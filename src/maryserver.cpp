#include "maryserver.h"

#include <windows.h>
#include <QDir>
#include <QDebug>
#include <QTime>
#include <QCoreApplication>
#include <QtGlobal>

const int MEGABYTE =  1048576;
const int MAX_MEMORY_FOR_MARY =  300; // 2000 megabytes
const int NORMAL_MEMORY_FOR_MARY = 1000; //1000 megabytes
const int MIN_MEMORY_FOR_MARY = 500; //500 megabytes

MaryServer::MaryServer()
{
    maryServerProcess = new QProcess();
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(restartMaryServer()));
    timer->start(1000);
}

MaryServer::~MaryServer()
{
    delete timer;
    maryServerProcess->close();
    delete maryServerProcess;
}

void MaryServer::start()
{
    double availableMemory;
    memoryForMaryServer = 0;

    maryServerProcess->close();

    availableMemory = getAvailableMemory();

    //if there is enough memory available we allocate it to mary server
    //otherwise we choose the default allocation
    if (availableMemory > MAX_MEMORY_FOR_MARY)
        //try to allocate 2Gb for mary server
        startMaryServerProcess(MAX_MEMORY_FOR_MARY);

    //if process has not started try with 1gb
    if  (maryServerProcess->processId() == 0) {
        if (availableMemory > NORMAL_MEMORY_FOR_MARY)
            //try to allocate 1Gb for mary server
            startMaryServerProcess(NORMAL_MEMORY_FOR_MARY);

    }

    if  (maryServerProcess->processId() == 0)
        //try to allocate 500mb for mary server
        startMaryServerProcess(MIN_MEMORY_FOR_MARY);

    //finally if process fails to start we display information
    if (maryServerProcess->processId() == 0) {
        memoryForMaryServer = 0;
        qDebug() << "Emily cannot start.";
    }
}

void MaryServer::restartMaryServer()
{
    if (maryServerProcess->processId() == 0)
        start();
}

double MaryServer::getAvailableMemory()
{
    MEMORYSTATUSEX memory_status;
    ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
    memory_status.dwLength = sizeof(MEMORYSTATUSEX);
    GlobalMemoryStatusEx(&memory_status);
    return (double) memory_status.ullAvailPhys/MEGABYTE;
}

void MaryServer::startMaryServerProcess(int memory)
{
    if  (maryServerProcess->processId() == 0) {
        //try to allocate memory for mary server
        QString directory = QDir::currentPath();
        directory.replace("/", "\\");
        QString string1 = "\"" + directory + "\\marytts\\bin\\jre\\bin\\java\" -ea -Xms40m -Xmx" + QString::number(memory) +"m -cp ";
        QString string2 = "\"" + directory + "\\marytts\\lib\\*\" \"-Dserver=http\" marytts.server.Mary\"";
        QString string3 = string1 + " " + string2;

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
        maryServerProcess->startCommand(string3);
#else
        maryServerProcess->start(string3);
#endif

        memoryForMaryServer = memory;

        //If mary server has started this delay does not affect anything
        //If mary server has not started this delay is necessary to catch the flaw in the next check
        delay(2);
    }
}

void MaryServer::delay(int n)
{
    QTime dieTime= QTime::currentTime().addSecs(n);
    while( QTime::currentTime() < dieTime )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

//#include <windows.h>
//#include "psapi.h"
// double MaryServer::usedMemory()
// {
//     HANDLE hProcess;
//     PROCESS_MEMORY_COUNTERS pmc;

//     if (maryServerProcess->processId() != 0)
//     {
//         hProcess = maryServerProcess->pid()->hProcess;
//         if ( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) )
//         {
//             qWarning() << (double) pmc.WorkingSetSize/MEGABYTE;
//             return (double) pmc.WorkingSetSize/MEGABYTE;
//             //CloseHandle( hProcess );
//         }
//     }
//     else
//         qWarning() << "nullptr process";

//     //error if control reaches here
//     return -1;
// }

