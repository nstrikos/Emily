#ifndef MARYSERVER_H
#define MARYSERVER_H

#include <QObject>
#include <QProcess>
#include <QTimer>

class MaryServer : public QObject
{
    Q_OBJECT
public:
    MaryServer();
    ~MaryServer();
    void start();
    double usedMemory();

private slots:
    void restartMaryServer();

private:
    double getAvailableMemory();
    void startMaryServerProcess(int memory);
    void delay(int n);

    QProcess *maryServerProcess;
    int memoryForMaryServer;
    QTimer *timer;
};

#endif // MARYSERVER_H
